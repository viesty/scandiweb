/*
    Usage:
    Just call addPlaceholders function targeting on the document or 'this' - $(document).addPlaceholders
    Doesn't matter if it is called when the document is ready or not - will work in both cases.
 */
(function ($) {
    "use strict";
    jQuery.fn.addPlaceholders = function () {
        this.ready(function ($) {
            var $elements = 'input, textarea';
            //check for support of placeholder attribute
            var $support = supportOfPlaceholder();
            //if placeholders are not supported (returns false), we are in an old browser
            if (!$support) {
                //initial search of input and textarea elements with placeholder attribute.
                //*textarea and input only as per the task*
                $(this).find($elements + '[placeholder]').each(function () {
                    var $field = $(this);
                    if ($field.val() === '') $field.val($field.attr('placeholder'))
                    //when user clicks on the input field, remove placeholder text
                    $($elements).focus(function () {
                        //can't cache $(this).val setting, because we need the exact object, not all of 'em
                        if ($field.val() === $field.attr('placeholder')) $(this).val('');
                    });
                    //if input text is empty when clicking out of the field, add placeholder text
                    $($elements).blur(function () {
                        if ($field.val() === '') $field.val($field.attr('placeholder'));
                    });
                    //remove placeholder text on submit if there is still one present
                    $($elements).parents('form').submit(function () {
                        var $parent = $(this);
                        $parent.find($elements + '[placeholder]').each(function () {
                            var $field = $(this);
                            if ($field.val() === $field.attr('placeholder')) $field.val('');
                        });
                    });
                });
            }
            return this;
        });
    };
    function supportOfPlaceholder() {
        //returns false if placeholder is not supported
        return 'placeholder' in document.createElement('input');
    }
})(jQuery);