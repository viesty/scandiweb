<?php
require_once 'classes/Db.php';
require_once 'classes/Products.php';
require_once 'classes/Disk.php';
require_once 'classes/Furniture.php';

/*
 * For furniture it is not obligatory to use Furniture class as everything is pulled from Products.php
 * For disks it is partly obligatory as there is need for a different name than just a title and appended 'MB' to
 * every instance of size.
 * So for furniture pulling it would be enough to initialize Products class and pull from it. However, using Furniture
 * class might be a bit less confusing.
 * To get disk information without anything appended one could fetch it either from Furnature or Products class
 * directly
 */

$disk = new Disk('localhost', 'root', 'viesturs123', 'scandi');
$furniture = new Furniture('localhost', 'root', 'viesturs123', 'scandi');

/*
 * Usage:
 *  Firstly call getAllAttributes(productID) to fetch attributes and then access them via:
 *  getProductName()      - gets the name of the product.
 *  getAttribute(x)    - gets one attribute value of a product. Params - attribute name
 */
$attributes = $disk->getAllAttributes(4);
var_dump($attributes);
echo "<br>";
echo $disk->getProductName();
echo "<br>";
echo $disk->getAttribute('attribute_2');
echo "<hr>";

var_dump($disk->getAllAttributes(2));
echo "<br>";
echo $disk->getProductName();
echo "<br>";
echo $disk->getAttribute('price');
echo "<br>";
echo $disk->getAttribute('attribute_1');
echo "<hr>";

$furniture->getAllAttributes(1);
echo $furniture->getProductName();
echo "<br>";
echo $furniture->getAttribute('attribute_2');
echo "<hr>";

var_dump($furniture->getAllAttributes(3));
echo "<br>";
echo $furniture->getProductName();
echo "<br>";
echo $furniture->getAttribute('attribute_1');
echo "<hr>";