<?php

class Disk extends Products
{
    function __construct($host, $user, $password, $db)
    {
        parent::__construct($host, $user, $password, $db);
    }

    function getProductName(){
        //Append the size attribute to the name. (also appends MB)
        return parent::getProductName()." ".$this->getAttribute('attribute_1');
    }

    function getAttribute($productAttribute){
        $result = parent::getAttribute($productAttribute);
        return $result;
    }

    function getAllAttributes($productId){
        parent::getAllAttributes($productId);
        //append MB to the size attribute inside the associative array
        $this->productAttributes['attribute_1'].='MB';
        return $this->productAttributes;
    }
}