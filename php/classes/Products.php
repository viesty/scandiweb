<?php

class Products extends Db
{

    public $productAttributes;

    function __construct($host, $user, $password, $db){
        parent::__construct($host, $user, $password, $db);
    }

    //method for getting products name by its ID
    function getProductName()
    {
        return $this->getAttribute('title');
    }

    //method for getting attribute by products ID
    function getAttribute($attr)
    {
        if(isset($this->productAttributes[$attr])){
            return $this->productAttributes[$attr];
        }else{
            return "No such attribute '".$attr."' exists for this product";
        }
    }
    // method for getting all the attributes of a product by its ID
    function getAllAttributes($productId){
        $productId = $this->sanitize($productId);
        if ($result = $this->query("SELECT * FROM products WHERE id=$productId")) {
            //return an associative array of all attributes
            $this->productAttributes = mysqli_fetch_assoc($result);
        } else {
            echo 'query error ' . $this->lastQuery;
        }
    }
}