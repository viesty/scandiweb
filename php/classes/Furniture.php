<?php


class Furniture extends Products
{
    /*
     * This class may as well be not used and all requests made to Products class.
     * However, using separate classes for disks and furniture might be more straightforward and less confusing
     */
    function __construct($host, $user, $password, $db){
        parent::__construct($host, $user, $password, $db);
    }

    function getProductName(){
        return parent::getProductName();
    }

    function getAttribute($productAttribute){
        return parent::getAttribute($productAttribute);
    }

    function getAllAttributes($productId){
        parent::getAllAttributes($productId);
        return $this->productAttributes;
    }
}